#!/usr/bin/python

import os, sys, datetime, platform
from flask import render_template
from flask import Flask
from time import sleep
app = Flask(__name__)

app_version='2.0'

@app.route('/ajax_cpu')
def ajax_cpu():
 last_idle = last_total = 0
 count=2
 while count>0:
  with open('/proc/stat') as f:
   fields = [float(column) for column in f.readline().strip().split()[1:]]
  idle, total = fields[3], sum(fields)
  idle_delta, total_delta = idle - last_idle, total - last_total
  last_idle, last_total = idle, total
  utilisation = 100.0 * (1.0 - idle_delta / total_delta)
  count-=1
  sleep(1)
 return('\r%5.1f%%' % utilisation)


@app.route('/')
@app.route('/index.html')
def hello_world():
 template_vars={'name': 'Michal', 'date': datetime.datetime.now(),'hostname': platform.node(), 'app_start_time':app_start_time,  'app_version': app_version}

 return render_template('index.html', vars=template_vars)


if __name__ == '__main__':
 app_start_time=datetime.datetime.now()
 app.run(host='0.0.0.0', port=8080, debug=True)
