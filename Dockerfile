FROM localhost/vxml_bin
USER root

ADD entrypoint.sh /
RUN chmod 755 /entrypoint.sh && chmod -R g+rwx /v3xml0

CMD /bin/bash -c "sleep infinity"

